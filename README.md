# PeopleAPI
A simple .NET Core 2.2 Web API with a local Sql Server Express database backend  
Leverages Dapper ORM for querying  
Follows the mediator architecture pattern  

This API is to be forked and used in conjunction with the homework document to provide a working solution. To work with the solution, 
you will need to install [Visual Studio Community 2019](https://visualstudio.microsoft.com/downloads/?utm_medium=microsoft&utm_source=docs.microsoft.com&utm_campaign=inline+link&utm_content=download+vs2019) and [.NET Core SDK 2.2](https://www.microsoft.com/net/download/all)

####To setup your local database:
1. Open the solution in VS Community 2019  
2. Select 'View' -> 'SQL Server Object Explorer'  
3. Right click 'SQL Server' and click 'Add SQL Server'  
4. Click 'Local', then 'MSSQLLocalDB', then click the 'Connect' button  
5. Expand the local server you just created, right click the 'Databases' folder, and click 'Add New Database'  
6. Enter 'Directory' as the 'Database Name' and click the 'OK' button  
7. Start the app. The DataInitializer class seeds data on startup if none exists.  

#####Technology Used:
https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-2.2&tabs=visual-studio  
https://github.com/jbogard/MediatR  
https://dapper-tutorial.net/  