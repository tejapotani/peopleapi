using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using PeopleApi.Messages.Commands;
using PeopleApi.Models;

namespace PeopleApi.Handlers.Command
{
    public class AddPersonHandlerAsync : IRequestHandler<AddPersonCommand, bool>
    {
        private readonly IDbConnection _dbConnection;

        public AddPersonHandlerAsync(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public Task<bool> Handle(AddPersonCommand request, CancellationToken cancellationToken)
        {
            try
            {

                var person = _dbConnection.Query<Person>(@"
                INSERT INTO dbo.Person
                VALUES (@FirstName, @LastName, @Address, @City, @State, @Zip)", request);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to add the person " + ex.Message);
            }

            return Task.FromResult(true);
        }
    }
}