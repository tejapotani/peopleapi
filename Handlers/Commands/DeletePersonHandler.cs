using Dapper;
using MediatR;
using PeopleApi.Messages.Commands;
using PeopleApi.Models;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace PeopleApi.Handlers.Command
{
    public class DeletePersonHandler : IRequestHandler<DeletePersonCommand, bool>
    {
        private readonly IDbConnection _dbConnection;

        public DeletePersonHandler(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public Task<bool> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var query = _dbConnection.Query(@"
                DELETE FROM dbo.Person
                WHERE Id = @Id", request);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to delete the person with Id: " + ex.Message);
            }

            return Task.FromResult(true);
        }
    }
}