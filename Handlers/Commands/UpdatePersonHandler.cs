using Dapper;
using MediatR;
using PeopleApi.Messages.Commands;
using PeopleApi.Models;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace PeopleApi.Handlers.Command
{
    public class UpdatePersonHandler : IRequestHandler<UpdatePersonCommand, bool>
    {
        private readonly IDbConnection _dbConnection;

        public UpdatePersonHandler(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public Task<bool> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var queryResult = _dbConnection.Query(@"
                UPDATE dbo.Person
                SET FirstName = @FirstName, 
                    LastName = @LastName, 
                    Address = @Address, 
                    City = @City, 
                    State = @State, 
                    Zip = @Zip
                WHERE Id = @Id", request);
            }
            catch (Exception ex)
            {
                throw new Exception("Person update failed." + ex.Message);
            }

            return Task.FromResult(true);

        }
    }
}