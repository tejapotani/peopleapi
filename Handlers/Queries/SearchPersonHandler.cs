﻿using Dapper;
using MediatR;
using PeopleApi.Messages.Queries;
using PeopleApi.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PeopleApi.Handlers.Queries
{
    public class SearchPersonHandler : IRequestHandler<SearchPeopleQuery, List<Person>>
    {
        private readonly IDbConnection _dbConnection;

        public SearchPersonHandler(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public Task<List<Person>> Handle(SearchPeopleQuery request, CancellationToken cancellationToken)
        {
            var numberOfSearchFields = 0;

            var query = @"SELECT Id, FirstName, LastName, Address, City, State, Zip
                    FROM dbo.Person
                    WHERE";

            var properties = request.GetType().GetProperties();
            foreach (var prop in properties)
            {
                var value = prop.GetValue(request);
                if (value != null)
                {
                    numberOfSearchFields++;
                    if (numberOfSearchFields > 1)
                    {
                        query += "AND";
                    }

                    query += $" {prop.Name} LIKE '%{value}%' ";
                }
            }

            var queryResult = _dbConnection.Query<Person>(query).OrderBy(x => x.LastName).ToList();

            return Task.FromResult(queryResult);
        }
    }
}