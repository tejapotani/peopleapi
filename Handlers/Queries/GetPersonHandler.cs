using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using PeopleApi.Messages.Queries;
using PeopleApi.Models;

namespace PeopleApi.Handlers.Queries
{
    public class GetPersonHandler : IRequestHandler<GetPersonQuery, Person>
    {
        private readonly IDbConnection _dbConnection;

        public GetPersonHandler(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public Task<Person> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            var queryResult = _dbConnection.Query<Person>(@"
                SELECT 
                    Id,
                    FirstName,
                    LastName,
                    Address,
                    City,
                    State,
                    Zip
                FROM dbo.Person
                WHERE Id = " + request.Id).FirstOrDefault();

            if (queryResult != null)
            {
                return Task.FromResult(queryResult);
            }
            else
            {
                throw new Exception("No person found with Id: " + request.Id);
            }
        }
    }
}